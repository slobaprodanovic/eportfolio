<?php
namespace ePortfolio\Controller;
require(__DIR__."/../Parameters.php");
/**
 * CREATE REQUIRE FOR PARAMETERS
 */
/**
 * [MailController description]
 * Singleton class for sending email
 */
class MailController
{
  protected static $instance;
  protected static $username   = "sender@slobodanprodanovic.info";
  protected static $password   = "MB4I6mzuhokw";
  protected static $message;
  protected static $transport;
  protected static $mailer;

  private function __construct()
  {
    /**
     * Configuration and setup of SwiftMailer
     */
     self::$message    = new \Swift_Message();
     self::$transport  = new \Swift_SmtpTransport("slobodanprodanovic.info", 465, "ssl");
     self::$transport->setUsername(self::$username);
     self::$transport->setPassword(self::$password);
     self::$mailer = new \Swift_Mailer(self::$transport);

    //Create a message
    //$message = new \Swift_Message();
    self::$message->setSubject('Poruka sa slobodanprodanovic.info');
    //$this->message->setFrom(['sender@slobodanprodanovic.info' => $name]);
    self::$message->setTo(['sloba.prodanovic@gmail.com'=> 'Slobodan Prodanovic']);
    //$this->message->setReplyTo([$email => $name]);
    // $this->message->setBody("<br /><b>Poruka od:</b> ".$name.". <br /><b>Sadržaj poruke:</b> ".$messageText, "text/html");
    // if($copy == "marked")
    // {
    //   $this->message->addCc($email, $name);
    // }
    //
    // //Send the message
    // $result = $this->mailer->send($message);
    //
    // return $result;
  }

  public static function getInstance()
  {
    if (is_null(self::$instance))
    {
      self::$instance = new self();
    }
    return self::$instance;
  }

  public static function sendMessage($name, $email, $messageText, $copy)
  {
    self::$message->setFrom(['sender@slobodanprodanovic.info' => $name]);
    self::$message->setReplyTo([$email => $name]);
    self::$message->setBody("<br /><b>Poruka od:</b> ".$name.". <br /><b>Sadržaj poruke:</b> ".$messageText, "text/html");

    /**
     * $copy is checkbox that user has determens
     * if copy of message needs to be send to
     * email address of message sender.
     * Posible values for $copy are "marked"
     * or "unmarked".
     * If variable == "marked", email address
     * of sender will be added to carbon copy
     * of email message.
     * @var $copy[string]
     */
    if($copy == "marked")
    {
      self::$message->addCc($email, $name);
    }

    //Send the message
    $result = self::$mailer->send(self::$message);

    return $result;
  }
}
