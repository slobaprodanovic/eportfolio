<?php
namespace ePortfolio\Controller;

use Symfony\Component\Translation\Translator;
use Symfony\Component\Translation\MessageSelector;
use Symfony\Component\Translation\Loader\ArrayLoader;
use Symfony\Component\Translation\Loader\YamlFileLoader;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bridge\Twig\Form\TwigRenderer;
use Symfony\Bridge\Twig\Form\TwigRendererEngine;
use ePortfolio\Controller\MailController;

/**
 * Controller for all pages
 * of the website.
 */
class DefaultController
{
  protected $request;
  protected $response;
  protected $locale;
  protected $loader;
  protected $twig;
  protected $translator;
  protected $formFactory;
  protected $langCookie;

  public function __construct()
  {
    $this->request  = Request::createFromGlobals();
    $this->response = new Response();
    $this->loader = new \Twig_Loader_Filesystem(__DIR__.'/../Templates');
    $this->twig = new \Twig_Environment($this->loader);

    $this->pageUrl();
    $this->twig->addGlobal("currentPage", $this->request->getPathInfo());

    // Send email with action when there is new visitor on website
    // $this->neuVisitor($this->request->getPathInfo());
  }

  protected function pageUrl()
  {
    $pageTemp = $this->request->getPathInfo();
    $pageEn   = substr_replace($pageTemp, "en", 1, 2);
    $pageRs   = substr_replace($pageTemp, "sr", 1, 2);

    $this->twig->addGlobal("pageEn", $pageEn);
    $this->twig->addGlobal("pageRs", $pageRs);
  }

  public function homepageAction(string $lang="en")
  {
    $this->languageSetup($lang);
    $this->response->setContent($this->twig->render('index.html.twig'));
    return $this->response;
  }

  public function aboutAction(string $lang)
  {
    $this->languageSetup($lang);
    $this->response->setContent($this->twig->render('about.html.twig'));
    return $this->response;
  }

  public function portfolioAction(string $lang)
  {
    $this->languageSetup($lang);
    $this->response->setContent($this->twig->render('portfolio.html.twig'));
    return $this->response;
  }

  public function contactAction(string $lang)
  {
    $this->languageSetup($lang);
    $this->response->setContent($this->twig->render('contact.html.twig'));
    return $this->response;
  }

  public function estudentAction(string $lang)
  {
    $this->languageSetup($lang);
    $this->response->setContent($this->twig->render('/portfolio/estudent.html.twig'));
    return $this->response;
  }

  public function eleagueAction(string $lang)
  {
    $this->languageSetup($lang);
    $this->response->setContent($this->twig->render('/portfolio/eleague.html.twig'));
    return $this->response;
  }

  public function efarmAction(string $lang)
  {
    $this->languageSetup($lang);
    $this->response->setContent($this->twig->render('/portfolio/efarm.html.twig'));
    return $this->response;
  }

  /**
   * [messageAction description]
   * API for form on contact pages
   * User sends name, his email, message
   * @return [response]
   * Method returns feedback code of
   * SwiftMailer send method.
   */
  public function messageAction()
  {
    $response = new JsonResponse();
    /**
     * Getting values from Ajax request
     */
    $name     = $this->request->request->get('name');
    $email    = $this->request->request->get('email');
    $message  = $this->request->request->get('message');
    $copy     = $this->request->request->get('copy');

    if ($name=="" || $email=="" || $message=="" || ($copy!="marked" && $copy!="unmarked"))
    {
      return new JsonResponse(array("error" => "Not Allowed"), 403);
    }

    $response->setContent(MailController::getInstance()->sendMessage($name, $email, $message, $copy));
    return $response;
  }

  protected function languageSetup(string $lang)
  {
    switch ($lang){
      case "en":
        $this->request->setLocale('en_US');
        break;
      case "sr":
        $this->request->setLocale('sr_RS');
        break;
      default:
        $this->request->setLocale('en_US');
        break;
    }

    $this->twig->addGlobal("language", $lang);

    $this->translator = new Translator($this->request->getLocale(), new MessageSelector());
    $this->translator->setFallbackLocale('sr_RS');

    $this->twig->addExtension(new \Symfony\Bridge\Twig\Extension\TranslationExtension($this->translator));

    $this->translator->addLoader('yaml', new YamlFileLoader());
    $this->translator->addResource('yaml', __DIR__.'/../Translation/messages.en.yml', 'en_US');
    $this->translator->addResource('yaml', __DIR__.'/../Translation/messages.sr.yml', 'sr_RS');
  }

  protected function neuVisitor(string $message)
  {
    MailController::getInstance()->sendMessage("Automated", "proda89@hotmail.com", $message, "unmarked");
  }
}
