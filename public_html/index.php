<?php
namespace ePortfolio;

require_once __DIR__.'/../vendor/autoload.php';

use Symfony\Component\Debug\Debug;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing;
use Symfony\Component\HttpKernel;
use ePortfolio\Controller\DefaultController;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Routing\Loader\YamlFileLoader;

Debug::enable();
//error_reporting(0);

$request  = Request::createFromGlobals();
$routes   = new Routing\RouteCollection();
$resolver = new HttpKernel\Controller\ControllerResolver();
$locator = new FileLocator(array(__DIR__."/../src/Routing/"));
$loader = new YamlFileLoader($locator);

$routes = $loader->load('Routes.yml');

$context = new Routing\RequestContext();
$context->fromRequest(Request::createFromGlobals());

$matcher = new Routing\Matcher\UrlMatcher($routes, $context);
$parameters = $matcher->match($request->getPathInfo());

  try {
      $request->attributes->add($matcher->match($request->getPathInfo()));

      $controller = $resolver->getController($request);
      $arguments = $resolver->getArguments($request, $controller);

      $response = call_user_func_array($controller, $arguments);
  } catch (Routing\Exception\ResourceNotFoundException $glupost) {
      $response = new Response('An error occurred'.$glupost, 404);
  } catch (\Exception $c) {
      $response = new Response('An error occurred'.$c, 500);
  }

$response->send();
