/**
 * Class for send and processing request
 * for repositories info from BitBucket API.
 */

class BitBucket
{
  constructor()
  {
      this.widgetMask();
      this.getRepositories(this.returnValue);
  }
  /**
   * Method contains callback method "returnValue"
   * This method sends Ajax request and if promise is fullfiled
   * (.done) returns one by one objects that containts info about
   * repositories.
   * If there is problem with getting data from the API, method will
   * return string "error".
   * @param  {[type]} returnValue [description]
   * @return {[type]}             [description]
   */
  getRepositories(returnValue)
  {
    $.ajax({
        type:'GET',
        url:'https://api.bitbucket.org/2.0/repositories/slobaprodanovic',
      }).done(function(msg)
      {
        let message,
            date,
            i;

            i=1;
            message = msg.values;

            message.forEach(function(entry)
            {
              let gitObject = new Object(),
                  timeTemp  =new Date(entry.updated_on);

                  gitObject.No          = i;
                  gitObject.name        = entry.name;
                  gitObject.description = entry.description;
                  gitObject.updateTime  = ('0' + timeTemp.getHours()).slice(-2)     + ':' +
                                          ('0' + timeTemp.getMinutes()).slice(-2)   + ' ' +
                                          ('0' + timeTemp.getDate()).slice(-2)      + '.' +
                                          ('0' + (timeTemp.getMonth()+1)).slice(-2) + '.' +
                                          timeTemp.getFullYear()+'.';
                  gitObject.link        = entry.links.html.href;
                  returnValue(gitObject);
                  i++;
            });
        }).fail(function(msg)
        {
          returnValue("error");
          console.log('There has been error with Ajax request to BitBucket.');
        });
  }

  /**
   * Creates table with headers for widget
   * @return {[type]} [description]
   */
  widgetMask()
  {
    $('#bitBucketWidget').append("<div class='row'>");
    $('#bitBucketWidget').append("<div class='col-sm-3 bbWidgetHeaders'>No</div>");
    $('#bitBucketWidget').append("<div class='col-sm-3 bbWidgetHeaders'>Repository</div>");
    $('#bitBucketWidget').append("<div class='col-sm-3 bbWidgetHeaders'>Description</div>");
    $('#bitBucketWidget').append("<div class='col-sm-3 bbWidgetHeaders'>DateTime</div");
    $('#bitBucketWidget').append("<hr class='widgetLine' />");
    $('#bitBucketWidget').append("</div>");
  }

  /**
   * Callback method.
   * Checks if Ajax has send successfull request.
   * If all is ok, adds received object values to the table.
   * @param  {[type]} value [description]
   * @return {[type]}       [description]
   */
  returnValue(value)
  {
    if (value === "error")
    {
      $('#bitBucketWidget').text("Nothing to show!");
    }else
    {
      $('#bitBucketWidget').append("<div class='row'>");
      $('#bitBucketWidget').append("<div class='col-sm-3 bbWidgetValues'>"+value.No+"</div>");
      $('#bitBucketWidget').append("<div class='col-sm-3 bbWidgetValues'>"+value.name+"</div>");
      $('#bitBucketWidget').append("<div class='col-sm-3 bbWidgetValues'>"+value.description+"</div>");
      $('#bitBucketWidget').append("<div class='col-sm-3 bbWidgetValues'>"+value.updateTime+"</div");
      $('#bitBucketWidget').append("</a>");
      $('#bitBucketWidget').append("</div>");
    }
  }
};
