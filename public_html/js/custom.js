$('document').ready(
  function() {
    let temp,
        bb,
        langSelect;

    temp = new Cookie();
    temp.testing();
    // bb = new BitBucket();
    // bb.getRepositories();

    langSelect = $("html").attr("lang");
    if (langSelect === "en_US" || langSelect === "en") {
      $('#langEng').addClass("langButtonActive");
    }else if(langSelect === "rs" || langSelect === "sr" || langSelect === "rs_SR" || langSelect === "sr_RS") {
      $('#langSrb').addClass("langButtonActive");
    }


  }
);


function cookieSet(cvalue)
{
    var cname  = "lang";
    var exdays = 30;
    var d = new Date();
    let langSelect;
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    location.reload();

}

function sendMessage ()
{
    let name,
        email,
        message,
        copy,
        error = 0;

        name    = document.getElementById("contactName").value;
        email   = document.getElementById("contactEmail").value;
        message = document.getElementById("contactMessage").value;
        copy    = document.getElementById("contactCopy").checked;

        if (copy === false)
        {
          copy = "unmarked";
        }else
        {
          copy = "marked";
        }

        if (name == null || name =="")
        {
          error = 1;
        }

        if (email == null || email =="")
        {
          error = 2;
        }

        if (message == null || message =="")
        {
          error = 3;
        }

        switch (error){
          case 0:
            send(name, email, message, copy);
            break;
          case 1:
            $('#messageFeedback').addClass('alert alert-danger animated fadeIn');
            $('#messageFeedback').text('Niste uneli ime!');
            break;
          case 2:
            $('#messageFeedback').addClass('alert alert-danger animated fadeIn');
            $('#messageFeedback').text('Niste uneli kontakt email!');
            break;
          case 3:
            $('#messageFeedback').addClass('alert alert-danger animated fadeIn');
            $('#messageFeedback').text('Niste uneli poruku!');
            break;
        }

        setTimeout(function ()
                    {
                      $('#messageFeedback').addClass('fadeOut');
                    }, 3000
                  );

        setTimeout(function ()
                    {
                      $('#messageFeedback').text('');
                      $('#messageFeedback').removeClass();
                    }, 6000
                  );
}

function send(name, email, message, copy)
{
    $.ajax({
        type:'POST',
        url:'/api/contact/message',
        data:'name='+name+'&email='+email+'&message='+message+'&copy='+copy,
        success:function(msg)
        {
            if(msg == '1' || msg == '2')
            {
              $('#messageFeedback').addClass('alert alert-success animated fadeIn');
              $('#messageFeedback').text('Poruka je poslata!');
            }else
            {
              $('#messageFeedback').addClass('alert alert-danger animated fadeIn');
              $('#messageFeedback').text('Poruka nije poslata! Greška: '+msg);
            }
        }
    });
}
